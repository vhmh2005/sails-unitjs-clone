User = require '../../api/models/User'
test = require 'unit.js'
clock = null

describe 'The User Model', ->
  it 'should run callback ', ->
    spy = test.spy()
    user =
      username : 'vhmh2005'
      password : 'password'
    test
      .when 'before user created', ->
        User.beforeCreate user, spy
      .wait 10, ->    # wait for User.beforeCreate completion
        user.password.must.not.equal 'password'
        spy.calledOnce.must.be.truthy()
    return
  return

#  it 'should run callback at least one', ->





#  it 'should hash the password before the user is created', (done)->
#    test.sinon.spy()
#    test
#    .when 'password is entered', ->
#    .then 'password should be hashed', ->
#    .and 'user is created', ->
#    User.beforeCreate
#      password : 'password', (err, user) ->
#        user.password.must.not.equal 'password'
#        done()
#        return
#    return