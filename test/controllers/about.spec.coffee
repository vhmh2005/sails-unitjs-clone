test = require 'unit.js'
AboutController = require '../../api/controllers/AboutController'

describe 'The About Controller', ->
  it 'should render the view when we load the about page', ->
    view = test.spy()
    test
      .when 'we load the about page', ->
        AboutController.index null, view:view
      .then 'the view should be rendered', ->
        view.called.must.be.truthy()
    return

  it 'should returned the Refunded items to stock', ->
    test
      .given 'a customer previously bought a black sweater from me', ->
        return
      .and 'I currently have three black sweaters left in stock', ->
        return
      .when 'he returns the sweater for a refund', ->
        return
      .then 'I should have four black sweaters in stock', ->
        {}.must.be.an.object()
    return
