module.exports = {
   express: {
   customMiddleware: function(app){
     console.log('info: loaded passport middleware into express');
     // This is very important in order to use passport in policy
     // Passport middleware must be initialize before router middleware is run
     // Check config/http.js for middleware loading order
     // These custom middlewares will be loaded into $custom placeholder in config/http.js
     app.use(passport.initialize());
     app.use(passport.session());
   }
 }
}
