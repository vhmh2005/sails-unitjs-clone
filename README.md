Sails v0.10 - Handlebars - Backbone
=============================

Latest version of sails integrated with handlebars and backbonejs

## Overview

This project is for a boilerplate framework based on the latest version of sails which is v0.10 beta at this moment. EJS is replaced by handlebars and backbonejs is added for single page web app built. Some other stuff is modified/added, such as passportjs and winston for logging

## Stack

Environment: Node.JS (on linux)
Language: javascript/coffeescript
Framework: sailsjs
Authentication: passport.js
Presentation: Handlebars
Unit Testing: unitjs
Logging: Winston
Browser framework: backbonejs